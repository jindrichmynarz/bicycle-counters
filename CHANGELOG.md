# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.8] - 2022-06-07

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

### Added

-   Implementation docs

## [1.0.7] - 2022-05-18

### Fixed

-   Fixed JSON validation schema

## [1.0.6] - 2022-05-02

### Changed

-   Temperature data type

## [1.0.5] - 2022-03-31

### Fixed

-   Bug - nulls in detections were saved in database as 0. Now detection value null is saved as NULL in db. [bicycle-counters#6](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/issues/6)

## [1.0.4] - 2021-12-06

### Changed

-   Mongoose validator to JSON validator ([bicycle-counters#3](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/issues/3))
