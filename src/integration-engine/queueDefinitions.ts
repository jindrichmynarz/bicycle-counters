import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { BicycleCounters } from "#sch/index";
import { BicycleCountersWorker } from "#ie/BicycleCountersWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: BicycleCounters.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + BicycleCounters.name.toLowerCase(),
        queues: [
            {
                name: "refreshCameaDataLastXHoursInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 14 * 60 * 1000, // 14 minutes
                },
                worker: BicycleCountersWorker,
                workerMethod: "refreshCameaDataLastXHoursInDB",
            },
            {
                name: "refreshCameaDataPreviousDayInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 1000, // 23 minutes
                },
                worker: BicycleCountersWorker,
                workerMethod: "refreshCameaDataPreviousDayInDB",
            },
            {
                name: "refreshCameaDataSpecificDayInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 5 * 60 * 1000, // 5 minutes
                },
                worker: BicycleCountersWorker,
                workerMethod: "refreshCameaDataSpecificDayInDB",
            },
            {
                name: "refreshEcoCounterDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 14 * 60 * 1000, // 14 minutes
                },
                worker: BicycleCountersWorker,
                workerMethod: "refreshEcoCounterDataInDB",
            },
            {
                name: "updateCamea",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 4 * 60 * 1000, // 4 minutes
                },
                worker: BicycleCountersWorker,
                workerMethod: "updateCamea",
            },
            {
                name: "updateEcoCounter",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 14 * 60 * 1000, // 14 minutes
                },
                worker: BicycleCountersWorker,
                workerMethod: "updateEcoCounter",
            },
        ],
    },
];

export { queueDefinitions };
