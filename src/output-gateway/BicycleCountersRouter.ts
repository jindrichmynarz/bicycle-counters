/**
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { toNumber } from "lodash";
import {
    IGeoJSONFeatureCollection,
    buildGeojsonFeatureCollection,
    parseCoordinates,
} from "@golemio/core/dist/output-gateway/Geo";
import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, paginationLimitMiddleware, pagination } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";

import { ILocation, IDetection, ILocationNormalized, models } from "./models";
import { BicycleCountersDetectionsModel } from "./models/BicycleCountersDetectionsModel";
import { BicycleCountersLocationsModel } from "./models/BicycleCountersLocationsModel";
import { BicycleCountersTemperaturesModel } from "./models/BicycleCountersTemperaturesModel";

export class BicycleCountersRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();

    protected BicycleCountersLocationsModel: BicycleCountersLocationsModel;
    protected BicycleCountersDetectionsModel: BicycleCountersDetectionsModel;
    protected BicycleCountersTemperaturesModel: BicycleCountersTemperaturesModel;

    public constructor() {
        super();
        this.BicycleCountersLocationsModel = models.BicycleCountersLocationsModel;
        this.BicycleCountersDetectionsModel = models.BicycleCountersDetectionsModel;
        this.BicycleCountersTemperaturesModel = models.BicycleCountersTemperaturesModel;
        this.initRoutes();
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);
            const data = await this.BicycleCountersLocationsModel.GetAll({
                lat: coords.lat,
                limit: toNumber(req.query.limit) || undefined,
                lng: coords.lng,
                offset: toNumber(req.query.offset) || 0,
                range: coords.range,
            });

            res.status(200).send(this.NormalizeLocations(data));
        } catch (err) {
            next(err);
        }
    };

    /**
     * get method for get BicycleCountersDetections or BicycleCountersTemperatures depending on given model
     */
    public GetData = (model: BicycleCountersDetectionsModel | BicycleCountersTemperaturesModel) => {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                // to make it a bit foolproof
                const isoDateTo: any = req.query.to ? new Date(req.query.to as string) : null;
                // imho we should limit date range
                const isoDateFrom = req.query.from ? new Date(req.query.from as string) : null;
                const aggregate = (req.query.aggregate as string) === "true";

                const data = await model.GetAll({
                    aggregate,
                    id: this.ConvertToArray(req.query.id || []),
                    isoDateFrom,
                    isoDateTo,
                    limit: toNumber(req.query.limit) || undefined,
                    offset: toNumber(req.query.offset) || 0,
                });

                data.forEach((element: IDetection) => {
                    if (aggregate) {
                        element.measured_from = (isoDateFrom || new Date("1970-01-01")).toISOString();
                        element.measured_to = (isoDateTo || new Date()).toISOString();
                        element.value = Math.round(element.value * 100) / 100;
                    } else {
                        element.measurement_count = 1;
                        element.measured_from = new Date(parseInt(element.measured_from, 10)).toISOString();
                        element.measured_to = new Date(parseInt(element.measured_to, 10)).toISOString();
                    }
                });

                res.status(200).send(data);
            } catch (err) {
                next(err);
            }
        };
    };

    /**
     * transforms raw DB output ILocation[] to desired IGeoJSONFeatureCollection
     * API output by grouping  directions by locations
     */
    private NormalizeLocations = (locations: ILocation[]): IGeoJSONFeatureCollection => {
        const normalizedData: ILocationNormalized[] = [];
        const indexes: { [key: string]: number } = {};
        locations.forEach((location: ILocation) => {
            if (indexes[location.id] !== undefined) {
                normalizedData[indexes[location.id]].directions.push({
                    id: location["directions.id"],
                    name: location["directions.name"],
                });
            } else {
                indexes[location.id] = normalizedData.length;
                normalizedData.push({
                    directions: [
                        {
                            id: location["directions.id"],
                            name: location["directions.name"],
                        },
                    ],
                    id: location.id,
                    lat: location.lat,
                    lng: location.lng,
                    name: location.name,
                    route: location.route,
                    updated_at: location.updated_at,
                });
            }
        });
        return buildGeojsonFeatureCollection(normalizedData, "lng", "lat", true);
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    private initRoutes = (expire?: number | string): void => {
        this.router.get(
            "/temperatures",
            [
                query("from").optional().isISO8601(),
                query("to").optional().isISO8601(),
                query("id").optional(),
                query("aggregate").optional().isBoolean(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("BicycleCountersRouter"),
            useCacheMiddleware(expire),
            this.GetData(this.BicycleCountersTemperaturesModel)
        );

        this.router.get(
            "/detections",
            [
                query("from").optional().isISO8601(),
                query("to").optional().isISO8601(),
                query("id").optional(),
                query("aggregate").optional().isBoolean(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("BicycleCountersRouter"),
            useCacheMiddleware(expire),
            this.GetData(this.BicycleCountersDetectionsModel)
        );

        this.router.get(
            "/",
            [query("latlng").optional().isLatLong(), query("range").optional().isNumeric()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("BicycleCountersRouter"),
            useCacheMiddleware(expire),
            this.GetAll
        );
    };
}

const bicycleCountersRouter: Router = new BicycleCountersRouter().router;

export { bicycleCountersRouter };
