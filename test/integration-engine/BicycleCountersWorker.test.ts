import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { BicycleCounters } from "#sch/index";
import { BicycleCountersWorker, CameaRefreshDurations } from "#ie";

describe("BicycleCountersWorker", () => {
    let worker: BicycleCountersWorker;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: Record<string, any>;
    let testMeasurementsData: number[];
    let testCameaMeasurementsTransformedData: Record<string, number[]>;
    let testEcoCounterMeasurementsTransformedData: any[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        config.datasources = {
            CountersEcoCounterTokens: {
                PRAHA: "",
            },
            BicycleCountersEcoCounterMeasurements: "",
            BicycleCountersCameaMeasurements: "",
            BicycleCountersStatPing: {
                startOffsetSec: 42,
            },
        };

        testData = [1, 2];
        testTransformedData = {
            directions: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            directionsPedestrians: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            locations: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
            locationsPedestrians: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
        };
        testMeasurementsData = [1, 2];
        testCameaMeasurementsTransformedData = {
            detections: [1, 2],
            temperatures: [1, 2],
        };
        testEcoCounterMeasurementsTransformedData = [
            {
                directions_id: null,
                locations_id: null,
                measured_from: new Date().valueOf(),
                measured_to: new Date().valueOf(),
                value: 1,
            },
        ];

        worker = new BicycleCountersWorker();

        sandbox.stub(worker["dataSourceCamea"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["dataSourceCameaMeasurements"], "getAll").callsFake(() => Promise.resolve(testMeasurementsData));
        sandbox.stub(worker["dataSourceEcoCounter"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["dataSourceEcoCounterMeasurements"], "getAll").callsFake(() => Promise.resolve(testMeasurementsData));
        sandbox.stub(worker["cameaTransformation"], "transform").callsFake(() => Promise.resolve(testTransformedData as any));
        sandbox
            .stub(worker["cameaMeasurementsTransformation"], "transform")
            .callsFake(() => Promise.resolve(testCameaMeasurementsTransformedData as any));
        sandbox
            .stub(worker["ecoCounterTransformation"], "transform")
            .callsFake(() => Promise.resolve(testTransformedData as any));
        sandbox
            .stub(worker["ecoCounterMeasurementsTransformation"], "transform")
            .callsFake(() => Promise.resolve(testEcoCounterMeasurementsTransformedData));

        sandbox.stub(worker, "sendMessageToExchange" as any).resolves();

        sandbox.stub(worker["locationsModel"], "save");
        sandbox.stub(worker["directionsModel"], "save");
        sandbox.stub(worker["detectionsModel"], "saveBySqlFunction");
        sandbox.stub(worker["temperaturesModel"], "saveBySqlFunction");
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + BicycleCounters.name.toLowerCase();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshCameaDataLastXHoursInDB method", async () => {
        await worker.refreshCameaDataLastXHoursInDB({});
        sandbox.assert.calledOnce(worker["dataSourceCamea"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["cameaTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["cameaTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["locationsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["directionsModel"].save as SinonSpy);
        sandbox.assert.calledTwice(worker["sendMessageToExchange"] as SinonSpy);
        testTransformedData.locations.map((f: any) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateCamea",
                JSON.stringify({ id: f.vendor_id, duration: CameaRefreshDurations.last3Hours })
            );
        });
        sandbox.assert.callOrder(
            worker["dataSourceCamea"].getAll as SinonSpy,
            worker["cameaTransformation"].transform as SinonSpy,
            worker["locationsModel"].save as SinonSpy,
            worker["directionsModel"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by refreshCameaDataPreviousDayInDB method", async () => {
        await worker.refreshCameaDataPreviousDayInDB({});
        sandbox.assert.calledOnce(worker["dataSourceCamea"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["cameaTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["cameaTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["locationsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["directionsModel"].save as SinonSpy);
        sandbox.assert.calledTwice(worker["sendMessageToExchange"] as SinonSpy);
        testTransformedData.locations.map((f: any) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateCamea",
                JSON.stringify({ id: f.vendor_id, duration: CameaRefreshDurations.previousDay })
            );
        });
        sandbox.assert.callOrder(
            worker["dataSourceCamea"].getAll as SinonSpy,
            worker["cameaTransformation"].transform as SinonSpy,
            worker["locationsModel"].save as SinonSpy,
            worker["directionsModel"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by refreshCameaDataSpecificDayInDB method", async () => {
        await worker.refreshCameaDataSpecificDayInDB({
            content: Buffer.from(
                JSON.stringify({
                    date: "2020-12-14",
                })
            ),
        });
        sandbox.assert.calledOnce(worker["dataSourceCamea"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["cameaTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["cameaTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["locationsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["directionsModel"].save as SinonSpy);
        sandbox.assert.calledTwice(worker["sendMessageToExchange"] as SinonSpy);
        testTransformedData.locations.map((f: any) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateCamea",
                JSON.stringify({
                    date: "2020-12-14",
                    duration: CameaRefreshDurations.specificDay,
                    id: f.vendor_id,
                })
            );
        });
        sandbox.assert.callOrder(
            worker["dataSourceCamea"].getAll as SinonSpy,
            worker["cameaTransformation"].transform as SinonSpy,
            worker["locationsModel"].save as SinonSpy,
            worker["directionsModel"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by updateCamea method (different geo)", async () => {
        await worker.updateCamea({
            content: Buffer.from(
                JSON.stringify({
                    duration: CameaRefreshDurations.last3Hours,
                    id: "BC_BS-BMZL",
                })
            ),
        });

        sandbox.assert.calledOnce(worker["dataSourceCameaMeasurements"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["cameaMeasurementsTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["cameaMeasurementsTransformation"].transform as SinonSpy, testMeasurementsData);

        sandbox.assert.calledOnce(worker["detectionsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledOnce(worker["temperaturesModel"].saveBySqlFunction as SinonSpy);
    });

    it("should calls the correct methods by refreshEcoCounterDataInDB method", async () => {
        await worker.refreshEcoCounterDataInDB({});
        sandbox.assert.calledOnce(worker["dataSourceEcoCounter"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["ecoCounterTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["ecoCounterTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["locationsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["directionsModel"].save as SinonSpy);
        sandbox.assert.callCount(worker["sendMessageToExchange"] as SinonSpy, 2);
        testTransformedData.directions.map((f: any) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateEcoCounter",
                JSON.stringify({
                    category: "bicycle",
                    directions_id: f.id,
                    id: f.vendor_id,
                    locations_id: f.locations_id,
                })
            );
        });
        sandbox.assert.callOrder(
            worker["dataSourceEcoCounter"].getAll as SinonSpy,
            worker["ecoCounterTransformation"].transform as SinonSpy,
            worker["locationsModel"].save as SinonSpy,
            worker["directionsModel"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by updateEcoCounter method (different geo)", async () => {
        await worker.updateEcoCounter({
            content: Buffer.from(
                JSON.stringify({
                    category: "bicycle",
                    directions_id: "ecoCounter-103047647",
                    id: "103047647",
                    locations_id: "ecoCounter-100047647",
                })
            ),
        });

        sandbox.assert.calledOnce(worker["dataSourceEcoCounterMeasurements"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["ecoCounterMeasurementsTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["ecoCounterMeasurementsTransformation"].transform as SinonSpy, testMeasurementsData);

        sandbox.assert.calledOnce(worker["detectionsModel"].saveBySqlFunction as SinonSpy);
    });
});
